# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import re
from graphviz import Digraph

class Promotion:
    """
    A Promotion is a set of students that have the same promotion year
    """
    dot_regex = r"subgraph (\d+) {[^}]*(rank=\"same\"|graph \[rank=same\])[^\"]*([^}]*)}"

    def __init__(self, year, students=[]):
        self.year = year
        self.students = students

    def add_student(self, student):
        self.students.append(student)

    """
    returns: the comment to put before the promotion subgraph in the .gv file
    """
    def get_comment(self):
        return "Promo " + str(self.year) + " ---------------------------------------------------"

class Student:
    dot_regex = r"\"(.*) (.*)\""
    dot_link_regex = r"\"([^\"]*)\" -> \"([^\"]*)\""

    def __init__(self, promo_year, firstname, lastname, promotions):
        self.promo_year = promo_year
        self.firstname = firstname
        self.lastname = lastname
        self.parents = []
        self.children = []
        self.insert_in_promotions(promotions)

    def __eq__(self, other):
        return self.get_label() == other.get_label()

    """
    Create a clone of this sutdent and adds them in a new promotion list
    args: new_promotions: list of Promotions to insert the clone into
    returns: the clone
    """
    def clone_into(self, new_promotions):
        return Student(self.promo_year, self.firstname, self.lastname, new_promotions)

    """
    args: parent: Student to add as parent
    """
    def add_parent(self, parent):
        self.parents.append(parent)
        if self not in parent.children:
            parent.add_child(self)

    """
    args: child: Student to add as child
    """
    def add_child(self, child):
        self.children.append(child)
        if self not in child.parents:
            child.add_parent(self)

    """
    returns: the label to display in the node
    """
    def get_label(self):
        return self.firstname + " " + self.lastname

    """
    Find the Promotion object in which this student belongs and add the student there, or create it if it doesn't exist.
    args: promotions: list of Promotions
    """
    def insert_in_promotions(self, promotions):
        for promo in promotions:
            if promo.year == self.promo_year:
                promo.add_student(self)
                return
        # If this line is reached, it means the promo could not be found : create it
        promotions.append(Promotion(self.promo_year, [self]))

    """
    Add this student as a node in the graph.
    args: graph: Graph/Digraph to which the student should be added. It can be a subgraph.
    """
    def add_to_graph(self, graph):
        graph.node(self.get_label())

    """
    Add this student's links to their children as edges in the graph.
    args: graph: Graph/Digraph to which the links should be added. Be careful not to give a subgraph with the attribute "rank=same" since it would place the children of the same rank as their parent.
    """
    def add_children_to_graph(self, graph):
        # Sort the children by first name for more consistency
        self.children.sort(key=lambda c: c.firstname)
        for child in self.children:
            if child.promo_year > self.promo_year:
                graph.edge(self.get_label(), child.get_label())
            else:
                # This link is unusual : the child is not from a younger promotion than the parent...display it as dashed
                graph.edge(self.get_label(), child.get_label(), _attributes=[["style", "dashed"]])

"""
Finds a student in a list of promotions using their label
args: label: string to use as label ; promotions: list of Promotions
"""
def find_student(label, promotions):
    for promo in promotions:
        for student in promo.students:
            if student.get_label() == label:
                return student

"""
Build the graph using the content of the promotions
args: promotions: list of Promotions
      graph: Graph/Digraph object to fill
"""
def build_graph(promotions, graph):
    # Sort the promotions by year for more consistency
    promotions.sort(key=lambda p: p.year)
    for promo in promotions:
        # Sort the people by first name for more consistency
        promo.students.sort(key=lambda p: p.firstname)
        # First put all of this promo's students in the promo subgraph...
        with graph.subgraph(name=str(promo.year), comment=promo.get_comment(), graph_attr=[["rank","same"]]) as promo_graph:
            for student in promo.students:
                student.add_to_graph(promo_graph)
        # ...then add the links to their children
        for student in promo.students:
            student.add_children_to_graph(graph)

"""
Parse the source of a graph (written in dot) and build a model out of it. Warning : does not handle comments.
args: graph_source: string containing the source
returns: a list of Promotions containing all the info
"""
def parse_promotions(graph_source):
    promotions = []
    # capture the students
    promos_capt = re.findall(Promotion.dot_regex, graph_source)
    for promo in promos_capt:
        # promo = (year, attributes, students)
        students_capt = re.findall(Student.dot_regex, promo[2])
        for student in students_capt:
            # student = (firstname, lastname)
            Student(int(promo[0]), student[0], student[1], promotions)
    # capture the links
    link_capt = re.findall(Student.dot_link_regex, graph_source)
    for link in link_capt:
        # link = (parent, child)
        parent = find_student(link[0], promotions)
        child = find_student(link[1], promotions)
        parent.add_child(child)
    return promotions
