# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import sys
from graphviz import Digraph
import model

INPUT_FILENAME = "imag_family.gv"

"""
Parse a graph source and extract the lineage of a student (i.e. their ascendance and descendance) then generate the corresponding graph.
"""
def main():
    if len(sys.argv) < 2:
        print("Command syntax : extract.py \"<first name> <last name>\"")
        exit()
    elif len(sys.argv) > 2:
        print("Too many arguments detected. Did you remember to put the student's name between quotes ? (ex: extract.py \"Obi-Wan Kenobi\")")
        exit()

    # parse the input graph
    root_label = sys.argv[1]
    with open(INPUT_FILENAME, encoding="utf-8", mode='r') as file:
        data = file.read()
    start_of_source = data.find("digraph ") # ignore the header comment
    promotions = model.parse_promotions(data[start_of_source:])

    # extract the lineage
    root = model.find_student(root_label, promotions)
    if root is None:
        print("Error : could not find student {}".format(root_label))
        exit()
    to_be_kept = [root] + get_descendants(root, promotions) + get_ascendants(root, promotions)
    promotions = extract(to_be_kept, promotions)

    # generate the output graph
    g = Digraph(root_label.replace(' ',''), graph_attr=[["ranksep", "2"]], strict=True, format="png")
    model.build_graph(promotions, g)
    g.view()
    return

"""
Extract a subset of students and the links between them.
args: to_be_kept: list of Students to keep
      promotions: list of Promotions containing everything we want to keep
returns: a new list of promotions containing only what we wanted to keep
"""
def extract(to_be_kept, promotions):
    # build a new list of promotions using only the desired students
    new_promotions = []
    for promo in promotions:
        for student in promo.students:
            if student in to_be_kept:
                # copy this student
                new_student = student.clone_into(new_promotions)
                # copy their children who are part of the genealogy
                for child in student.children:
                    if child in to_be_kept:
                        new_student.add_child(child)
    return new_promotions

"""
args: student: Student to use as root for the search
      promotions: list of Promotions to search
returns: all the descendants of the root, except through unusual links
"""
def get_descendants(student, promotions):
    desc = []
    for child in student.children:
        if child.promo_year > student.promo_year: # avoid cycles
            desc += [child]
            desc += get_descendants(child, promotions)
    return desc

"""
args: student: Student to use as root for the search
      promotions: list of Promotions to search
returns: all the ascendants of the root, except through unusual links
"""
def get_ascendants(student, promotions):
    asc = []
    for parent in student.parents:
        if parent.promo_year < student.promo_year: # avoid cycles
            asc += [parent]
            asc += get_ascendants(parent, promotions)
    return asc

if __name__ == "__main__":
    main()
